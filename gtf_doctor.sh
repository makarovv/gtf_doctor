#!/bin/bash

#PBS -N DoctorGTF
#PBS -q serial
#PBS -l walltime=24:00:00
#PBS -o o
#PBS -e o

## that will import cufflinks module to run on minerva



#### ##### #####          MODIFY AS NEEDED        ##### ##### ##### ##### #####

## Non-MSSM users, comment this line
module load cufflinks/2.0.0
module load BEDTools/2.16.2

wrkgdir='/gs01/projects/buxbaj01a/programs/gtf_doctor'
knowngtf='/gs01/projects/buxbaj01a/programs/gtf_doctor/gencode.v11.annotation.b37.gtf'
referencefa='/gs01/projects/buxbaj01a/resources/gatk/human_g1k_v37.fasta'
## name of original "X.gtf" file will be ovewritten by the "X.doctored.gtf"
outfile=${knowngtf}

#                 NO NEED TO MODIFY BELOW THIS LINE
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

# That will produce file called "cuffcmp.combined.gtf" in your working directory
# which contains the tss_id and p_id as descrbed at http://compbio.mit.edu/cummeRbund/faq.html

echo "Running cuffcompare to add the appropriate attributes to your custom GTF file ${knowngtf}"
cuffcompare  -o ${wrkgdir}/cuffcmp -r ${knowngtf} -s ${referencefa} ${knowngtf} ${knowngtf}

echo "Parsing attributes"

## Parse INFO field from the GTF file
echo "Parsing tss_id and p_id fields from the cuffcmp.combined.gtf "
awk 'BEGIN { FS = "\t" } ; { print $9 }' ${wrkgdir}/cuffcmp.combined.gtf > ${wrkgdir}/cuffcmp.combined.gtf.tmp

## Parse tss_id
awk 'BEGIN { FS = ";" } ; { print $5, $8, $9 }'  ${wrkgdir}/cuffcmp.combined.gtf.tmp | sort | uniq > ${wrkgdir}/tss_p_id.txt
perl -pi -e 's/ oId //g' ${wrkgdir}/tss_p_id.txt
perl -pi -e 's/  tss_id/\ttss_id/g' ${wrkgdir}/tss_p_id.txt
perl -pi -e 's/  p_id/; p_id/g' ${wrkgdir}/tss_p_id.txt

## cleanup
rm ${wrkgdir}/cuffcmp.*

echo "Parsing done, tmp files deleted "

## read through GTF file and add tss_id and p_id to the transcripts
python ${wrkgdir}/insert_tss_id.py ${knowngtf} ${wrkgdir}/tss_p_id.txt ${outfile}

## generate GC content for exons in each transcript
python ${wrkgdir}/gc_content_exons.py ${knowngtf} ${referencefa}

##started from here


#bedtools getfasta -name -tab -s -fi /share/bio/resources/human_g1k_v37.fasta -bed b.bed -fo b.fasta

#fastaFromBed -name -tab -s -fi /share/bio/resources/human_g1k_v37.fasta -bed b.bed -fo b.fasta
