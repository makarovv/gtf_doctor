#!/usr/bin/env python


################################################################################
#   June 7, 2012
#   Authors: Vlad Makarov
#   Language: Python
#   OS: UNIX/Linux, MAC OSX
#   Copyright (c) 2012, The Mount Sinai School of Medicine

#   Available under BSD  licence

#   Redistribution and use in source and binary forms, with or without modification,
#   are permitted provided that the following conditions are met:
#
#   Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
#
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#   IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
#   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#   BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
#   OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################


import sys
import os.path
import os
import gzip


""" Determines if the file is zipped
    and returns appropriate file handler """
def getFh(filename):
  fh = open(filename, "r")
  if filename.endswith('gz'):
    fh = gzip.open(filename, 'rb')
  return fh

""" converts format chr1,chr2,chr3,chr4,chr5...chrM to 1,2,3,4,5...MT"""
def hg19toB37(line):
    fields=line.split('\t')
    ## chrM to MT
    if fields[0].strip()=='chrM' or fields[0].strip()=='M':
        fields[0]='MT'
    ## append chr to all chromosomes
    else:
        if fields[0].strip().startswith("chr")==True:
            fields[0]=fields[0].replace('chr', '')

    print '\t'.join(fields)



""" converts format 1,2,3,4,5...MT to chr1,chr2,chr3,chr4,chr5...chrM """
def b37tohg19(line):
    fields=line.split('\t')
    ## MT to M
    if fields[0].strip()=='MT' or fields[0].strip()=='chrMT':
        fields[0]='chrM'
    ## append chr to all chromosomes
    else:
        if fields[0].strip().startswith("chr")==False:
            fields[0]="chr"+fields[0]

    print '\t'.join(fields)



""" specify file and define which way to go
    1 - B37 to HG19
    2 - (or anything else) HG19 to B37
"""
def convert(filename, whichway='1'):
    fh=getFh(filename)
    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            print line
        else:
            if whichway=='1':
                hg19toB37(line)
            else:
                b37tohg19(line)
    fh.close()



## Run or print usage and bail
if (len(sys.argv) > 1):
  whichway='1'
  filename=sys.argv[1]
  convert(filename=filename, whichway=whichway)
else:
  print ('gtf_doctor: Utility to add tss_id and p_id tags to GTF file, calculate GC content and convert GTF, GFF, BED and VCF files between b37 and hg19 formats')
  print ('usage:    python G19toB37.py [gtf_file] >  [out_gtf_file]')
