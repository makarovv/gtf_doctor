# Utility to add necessary tags to GTF file and convert GTF and VCF files between b37 and hg19 formats


Preamble: As Cuffdiff requires that the 'tss_id' and 'p_id' attributes be present in the reference .gtf
we created the tool which alows to insert the required tags to the latest GENCODE (http://www.gencodegenes.org/releases/) (or any other) GTF file.
For details about tags see http://compbio.mit.edu/cummeRbund/faq.html

Thus, the latest GENCODE file can be consistently used through tophat, cufdiff and RNA-SeQC (https://confluence.broadinstitute.org/display/CGATools/RNA-SeQC)

The package includes shell script and python code

To download:
git clone https://makarovv@bitbucket.org/makarovv/gft_doctor.git

You will find the following:

    'gtf_doctor.sh' - wrapper shell to run the tool. Runs cuffcompare to generate 'tss_id' and 'p_id' attributes

    'insert_tss_id.py' - mergers cuffcmp.combined.gtf file produced by cuffcompare with the original GENCODE (or other file)

    'HG19toB37.py' - helper tool to convert format chr1,chr2,chr3,chr4,chr5...chrM to 1,2,3,4,5...MT

    'B37toHG19.py' - helper tool to convert format chr1,chr2,chr3,chr4,chr5...chrM to 1,2,3,4,5...MT

    'gc_content_exons.py' - helper tool to calculate GC content for exons in each transcript in supplied GTF file. Also included to gtf_doctor.sh



To pring run instructions, run the script without arguments. HG19toB37.py and B37toHG19.py can also be applied to VCF, BED and GFF files


In the header section of the "gtf_doctor.sh" change the following:

    wrkgdir='/path/to/working_dir'

    knowngtf='/path/to/gencode.v11.annotation.b37.gtf'

    referencefa='/path/to/human_g1k_v37.fasta'



No other changes are required.

It is assumed that all shell and python files are saved at the same folder and you have Cufflinks installed and added to your PATH


#MSSM users:

    Utility can work at minerva (mssm high performance cluster)

    Simply modify arguments (wrkgdir, knowngtf, referencefa) as above and submit using "qsub gtf_doctor.sh"
